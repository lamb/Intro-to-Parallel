CS 431/531
Lab 7
https://gitlab.com/lamb/Intro-to-Parallel/raw/master/Lab7/Lab7.txt
Programming with OpenACC

--------- Instruction --------- 

Inspired by OpenMP, OpenACC is another directive-based programmign approach. Unlike OpenMP, OpenACC is focused on accelerator devices, primarly GPUs. OpenACC was created as a high-level alternative to low-level GPU programming languages like CUDA or OpenCL. 

There are several good OpenACC tutorials online:

  https://www.openacc.org/sites/default/files/inline-files/OpenACC_2.5_ref_guide_1.pdf
  http://on-demand.gputechconf.com/gtc/2013/presentations/S3076-Getting-Started-with-OpenACC.pdf
  https://www.youtube.com/playlist?list=PL5B692fm6--u-tdH8ct52nXWqmmp4B3C-

1. OpenACC Parallel Construct

  #pragma acc parallel
  {
    //  code here executed on accelerator
  }

  - see .pdf

----
2. OpenACC Loop Construct 
  A loop construct applies to the immediately following loop or tightly nested loops, and describes the type of device parallelism to use to execute the iterations of the loop.

  #pragma acc parallel
  {
    #pragma acc loop
    for (...)
  }

  Like OpenMP, these can be combined if the parallel region consists of a single loop

  #pragma acc parallel loop
  for (...)

----
3. The CPU and Accelerator can have separate memory hardware, so we need to manage the data movement. They way this data movement is handled can have a big impact on performance.

  Data clauses:
    copy()
    copyin()
    copyout()
    create()

  #pragma acc parallel copyin(input[]) copyout(output[])
  ...

  #pragma acc parallel copy(data[]) 
  ...

  #pragma acc parallel copyin(input[]) create(intermediate[]) copyout(output[])
  ...

----
4. Many accelerators are multi-threaded, and may have a type of thread hierarchy. For example, Nvidia GPU Architectures abstractly organize threads into grids, blocks, and individual threads. 
  OpenACC captures this nested parallelism by using the "gang" and "worker" constructs. In the OpenACC execution model, parallel regions consist of gangs running concurrently, with each gang containing a number of workers. These can be set as follows:

  #pragma acc parallel num_gangs(10) num_workers(32) 
  {
    #pragma acc loop gang
    for (int i = 0; i < N; ++i) {
    
      #pragma acc loop worker
      for (int j = 0; j < N; ++j) {
        ... 
      }
    }
  }

Parallel construct defined: A parallel construct launches a number of gangs executing in parallel, where each gang may support multiple workers, each with vector or SIMD operations.

5. OpenACC Kernels Construct 
  The kernels construct is a simplification of the parallel construct. Within a kernels region, there is no need to specify loops to be parallelized, as this is done by the compiler. The main idea is to let the compiler do more of the work, and simplify the programming.

  #pragma acc kernels
  {
    // parallelism determined by compiler
  }

  - see .pdf

6. OpenACC Runtime Library routines
    acc_get_num_devices( devicetype )
    acc_set_device_type( devicetype )
    acc_init( devicetype )
    acc_shutdown( devicetype )
  

--------- Exercise --------- 

Compile and Test OpenACC Matrix Multiplication

1. Download the following:
    https://gitlab.com/lamb/Intro-to-Parallel/raw/master/Lab7/matmul_kernels.cpp
    https://gitlab.com/lamb/Intro-to-Parallel/raw/master/Lab7/matmul_parallel.cpp

2. Load the pgi module for access to the pgc++ compiler.

    module load pgi/17

3. Compile and test the "kernels" implementation, targeting the host device

  pgc++ -std=c++11 -acc -mp -Minfo=all -ta=host -o matmul_kernels matmul_kernels.cpp

4. Compile and test the "kernels" implementation, targeting the GPU device (on Cerberus)

  pgc++ -std=c++11 -acc -mp -Minfo=all -ta=tesla -o matmul_kernels matmul_kernels.cpp

5. Test the versions targeting the GPU with different sizes (SIZE = 800, 1600, 2000).

6. Test the kernels version against the parallel version
  pgc++ -std=c++11 -acc -mp -Minfo=all -ta=tesla -o matmul_parallel matmul_parallel.cpp 
