// module load mpi/openmpi-x86_64
// mpic++ -std=c++11 -o mpi_max mpi_max.cpp 
// mpirun -np 4
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>

#include <chrono>
#include <thread>

#define  MASTER   0

int main (int argc, char *argv[])
{
  int   numtasks, taskid, len;
  char hostname[MPI_MAX_PROCESSOR_NAME];

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
  MPI_Comm_rank(MPI_COMM_WORLD,&taskid);
  MPI_Get_processor_name(hostname, &len);

  srand(time(NULL) + taskid);

  int round = 0;
  int flag = 0;
  int threshold = 500;

  // keep generating numbers until we get one above the threshold
  while (1) {

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    
    int my_rand = rand() % 1000;

    printf("Task: %d, Round: %d, Number: %d\n", taskid, round, my_rand);

    int global_max;

    // store the largest number in global_max
    MPI_Reduce(&my_rand, &global_max, 1, MPI_INT, MPI_MAX, MASTER, MPI_COMM_WORLD);

    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    if (taskid == MASTER) {
      printf("MASTER: Max number for Round %d: %d\n", round, global_max); 

      if (global_max < threshold) {
        printf("MASTER: Threshold of %d met by max number %d\n", threshold, global_max);
        flag = 1;
      }
    }

    // broadcast the results to all MPI Ranks
    MPI_Bcast(&flag, 1, MPI_INT, MASTER, MPI_COMM_WORLD);

    if (flag) {
      printf("Task %d finished\n", taskid);
      break;
    }

    round++;
  }

  MPI_Finalize();
}
