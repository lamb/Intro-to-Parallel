#!/bin/bash
##SBATCH --partition=short     ### Partition
#SBATCH --job-name=hello     ### Job Name
#SBATCH --time=1:00:00      ### WallTime
#SBATCH --nodes=1            ### Number of Nodes
#SBATCH --ntasks-per-node=4 ### Number of tasks (MPI processes)
##SBATCH --cpus-per-task=4
#SBATCH --mail-type=BEGIN,END,FAIL,TIME_LIMIT
#SBATCH --mail-user=ryelle@uoregon.edu

echo Hello from `hostname`
 
module load intel/17
module load mpi/openmpi-2.1.0 
which mpirun

mpirun -np 4 ./a.out
