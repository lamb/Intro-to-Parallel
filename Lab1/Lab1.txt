CS 431/531
Lab 1
https://gitlab.com/lamb/Intro-to-Parallel/raw/master/Lab1/Lab1.txt
Tools, Libraries, Modules, Compilers, Job Submission

--------- Instruction --------- 
0. Request NIC Account Access 

1. How to Access Remotely

  ssh jlambert@cerberus.nic.uoregon.edu
    - Login in to remote machines
    - Use with "-o ServerAliveInterval=30" flags to prevent timeouts

  scp username@hostname:source destination
    - Copy files between local (personal) and remote machines
    - Use with "-r" flag to copy entire directories

2. Module System
  module avail
    - List all modules available on the system
    - Use "module avail [prefix]" to list available modules with the prefix

  module load [module_name] 
    - Load module with name specified name 

  module list
    - List all currently loaded modules

  module purge
    - Remove all currently loaded modules
    - Use "module purge [module_name]" to purge specific modules

3. Compilers
  gcc/g++
   - GNU C/C++ compiler
   - Used for general C/C++ programming

  nvcc
    - Nvidia C/C++ compiler
    - Needed for CUDA programming

  icc
    - Intel C/C++ compiler
    - "mpicc" is the MPI wrapper for the icc compiler

4. Job Submission
  sinfo
    - Report the state of partitions and nodes

  srun [binary]
    - Run a parallel job on cluster managed by SLURM. 

  sbatch [scrpit]
    - Submit a batch script to SLURM. 

  squeue
    - View information about jobs located in the SLURM scheduling queue.
  
  scontrol
    - View and modify SLURM configuration and state.
  
  scancel
    - Signal or cancel jobs or job steps that are under the control of SLURM.

--------- Exercise --------- 
  Part 1 - Remote Access:
    0. Request/Receive access to NIC systems

    1. Use ssh to log into Cerberus (cerberus.nic.uoregon.edu)

    2. Download mpi-example.c to personal computer.

      https://gitlab.com/lamb/Intro-to-Parallel/raw/master/Lab1/mpi-example.c

    3. Use scp to copy mpi-example.c to your home directory on cerberus

  Part 2 - Modules and Compiling:
    1. Load the following modules:
      
        intel/17
        mpi/openmpi-2.1.0
	      slurm

    2. Use mpicc to compile example.c (leave executable name as a.out) 

    3. Execute a.out in the following two ways: traditionally, using ./a.out, and using srun.

  Part 3 - Batch Job:
    4. Download and scp slurm.sh to your home directory on cerberus

      https://gitlab.com/lamb/Intro-to-Parallel/raw/master/Lab1/mpi-example.c

    5. Begin a batch job using the sbatch command

    6. Review the results in slurm-[job_number].out
