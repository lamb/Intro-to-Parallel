CS 431/531
Lab 6
https://gitlab.com/lamb/Intro-to-Parallel/raw/master/Lab6/Lab6.txt

We will use the following program for the demonstration in this Lab:

  https://gitlab.com/lamb/Intro-to-Parallel/raw/master/Lab6/mpi_hello.c

----- Single Node

1. Load openmpi
module load openmpi

2. Compile using mpicc
mpicc -o hello mpi_hello.c

3. Execute normally
./hello

4. Execute using mpirun
mpirun -n 4 hello

------ Multi Node

1. Load slurm
moudle load slurm

2. Run with srun
srun hello

3. Run mpirun with srun
srun mpirun hello

4. Try srun with different configurations

srun --pty --nodes=1 --ntasks=4 mpirun hello
srun --pty --nodes=2 --ntasks=4 mpirun hello
srun --pty --nodes=2 --ntasks=8 mpirun hello
srun --pty --nodes=4 --ntasks=4 mpirun hello
