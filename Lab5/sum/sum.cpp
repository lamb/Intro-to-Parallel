#include <chrono>
#include <iostream>

#include <stdio.h>
#include <stdlib.h>

typedef std::chrono::high_resolution_clock Clock;

int main (int argc, char *argv[]) 
{
  int   i, n;
  float a[10000], b[10000], sum; 

  /* Some initializations */
  n = 10000;
  for (i=0; i < n; i++)
    a[i] = b[i] = i * 1.0;
  sum = 0.0;

  auto t1 = Clock::now();

  for (i=0; i < n; i++)
    sum = sum + (a[i] * b[i]);

   auto t2 = Clock::now();

  printf("   Sum = %f\n",sum);
  std::cout << "Runtime: "
    << std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count()
    << " nanoseconds" << std::endl;
}
